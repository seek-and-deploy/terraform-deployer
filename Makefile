CONFIG="/tmp/foo"
DIR:=$(shell dirname ${CONFIG})

.PHONY: help clean clean-pyc clean-build list test test-all coverage docs release sdist


help:
	@echo "build - run 'python setup.py build'"
	@echo "clean-build - remove build artifacts"
	@echo "clean-pyc - remove Python file artifacts"
	@echo "clean - clean-{build,pyc}"
	@echo "config - run configure-aws.sh. WARNING- Don't do this!"
	@echo "install - run 'python setup.py install', will run 'make build'"
	@echo "lint - check style with flake8"
	@echo "unit-test - run tests quickly with the default Python"
	@echo "testall - run tests on every Python version with tox"
	@echo "coverage - check code coverage quickly with the default Python"
	@echo "docs - generate Sphinx HTML documentation, including API docs"
	@echo "sdist - package"

guard-%:
	@ if [ "${${*}}" == "" ]; then \
		echo "Environment variable $* not set"; \
		exit 1; \
	fi

clean: clean-build clean-pyc clean-test uninstall

uninstall:
	pip uninstall -y deployer

clean-build:
	rm -rf build/
	rm -rf dist/
	rm -rf *.egg-info
	rm -rf coverage.xml clonedigger.xml junit.xml
	rm -rf htmlcov
	rm -f deployer/.version

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +

clean-test:
	rm -rf .tox/
	rm -rf .pytest*/
config:
	mkdir -p ${DIR}
	echo '[default]' > ${DIR}/config
	echo 'region = us-east-1' >> ${DIR}/config
	echo '' >> ${DIR}/config
	echo '[default]' > ${DIR}/credentials
	echo 'aws_access_key_id=XXXXXXXXXXXXXXXXXXXX' >> ${DIR}/credentials
	echo 'aws_secret_access_key=YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY' >> ${DIR}/credentials
	echo '' >> ${DIR}/credentials
	echo '[tests-random]' >> ${DIR}/credentials
	echo 'aws_access_key_id=XXXXXXXXXXXXXXXXXXXX' >> ${DIR}/credentials
	echo 'aws_secret_access_key=YYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYYY' >> ${DIR}/credentials
	echo '' >> ${DIR}/credentials
	cat ${DIR}/credentials

build: 
	pip install -r requirements.txt
	python setup.py build

install: build
	python setup.py install

install-dev: build
	pip install -e .

lint:
	flake8

unit-test:
	pytest --disable-warnings tests/unit

integration-test:
	pytest --disable-warnings tests/integration

testall:
	tox

coverage:
	coverage run --source neo_core setup.py test
	coverage report -m
	coverage html

docs:
	rm -f docs/neo.rst
	rm -f docs/modules.rst
	sphinx-apidoc -o docs/ neo
	$(MAKE) -C docs clean
	$(MAKE) -C docs html

sdist: clean
	python setup.py sdist
	ls -l dist

bdist: clean
	python setup.py bdist_wheel --universal

wheel: bdist

gitlab-unit-tests:
	gitlab-runner exec docker unit_tests

gitlab-integ-tests:
	gitlab-runner exec docker integration_tests

gitlab-build-wheel:
	gitlab-runner exec docker build_wheel

gitlab-test-wheel:
	gitlab-runner exec docker test_wheel

