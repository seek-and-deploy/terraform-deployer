try:
    from setuptools import setup, find_packages
    # import importlib
    # import importlib.util as util
    # import sys

    # from importlib import util
    # from importlib.util import find_spec

    # from importlib import find_module, load_module
except ImportError:
    from distutils.core import setup


URL = "https://gitlab.com/seek-and-deploy/terraform-deployer"
doclink = "Please visit {}.".format(URL)
# spec = util.find_spec('deployer._version')
# _version = util.module_from_spec(spec)
# sys.modules['_version'] = _version
# spec.loader.exec_module(_version)
#print("Version: ", _version.__version__)
# spec = importlib.util.find_spec('deployer')
# spec.loader.load_module()

setup(
    name='deployer',
    use_scm_version=True,
    setup_requires=['setuptools_scm'],
    description='Seek And Deploy Terraform Deployer',
    long_description=doclink,
    author='Paul Lussier',
    author_email='pllsaph+gitlab@gmail.com',
    url=URL,
    packages=find_packages(),
    scripts=['bin/deployer',
             'bin/gen_config',
             'bin/configure-aws.sh'],
    package_dir={'deployer': 'deployer'},
    package_data={'deployer': ['conf/logging.conf',
                               'conf/default_schema.json',
                               'conf/terraform_schema.json'
                               ]},
    include_package_data=True,
    install_requires=[
        'argparse',
        #'botocore<1.17.0,>=1.16.17',
        'botocore>=1.15.22',
        'boto3',
        'cryptography',
        'deep-merge',
        'docopt',
        'flake8',
        'jinja2',
        'jsonschema==4.1.2',
        'ruamel.yaml',
        'termcolor',
        'workdir',
    ],
    license="VERACODE",
    zip_safe=False,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)

