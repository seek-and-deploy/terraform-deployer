from importlib.metadata import version, PackageNotFoundError
from logging.config import logging
import os
import sys


try:
    __version__ = version(__name__)
    
except PackageNotFoundError:
    # package is not installed
    pass

__author__  = 'Veracode'
__appname__ = 'seek-and-deploy'
__email__   = 'pllsaph+github@gmail.com'
__prog__    = os.path.basename(sys.argv[0])
__version_string__ = "{app} {prog} {version}".format(app     = __appname__,
                                                     prog    = __prog__,
                                                     version = __version__)


try:  # Python 2.7+
    from logging import NullHandler
except ImportError:
    class NullHandler(logging.Handler):
        def emit(self, record):
            pass

logging.getLogger(__name__).addHandler(NullHandler())
path = os.path.join(os.path.dirname(__file__), 'conf', 'logging.conf')
logging.config.fileConfig(path)
