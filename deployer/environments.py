# -*- coding: utf-8 -*-
#
# Copyright Veracode Inc., 2014
import json
import logging
import os
import sys
from termcolor import colored

from   deployer import aws
from   deployer import s3
from   deployer import utils
from   deployer.terraform  import Terraform
from   deployer.exceptions import EnvironmentExistsException

logger = logging.getLogger(os.path.basename('deployer'))
tf = Terraform()


class Environment(object):
    def __init__(self, config = None):
        self.environment = None
        self.env_name    = None
        self.env_version = None

        if config:
            self.config = config
            self.environment = config.get('environment')
            self.env_name    = self.environment.get('name')
            self.env_version = self.environment.get('version')
            # Instantiate remote state only if:
            # - the terraform code isn't already checked out
            # - and we haven't already run 'terraform remote config...'
            if not os.path.isfile(os.path.join(config['tf_root'],
                                               '.terraform',
                                               'terraform.tfstate')):
                log_msg = "Configuring terraform remote state in: {}"
                logger.debug(log_msg.format(config['tf_root']))
                tf_command = tf.Command('init', config)
                return_code = utils.run_command(tf_command,
                                                cwd=config['tf_root'])
                if return_code != 0:
                    msg = f"{tf_command} failed with code {return_code}."
                    raise BaseException(msg)

            # Grab all the tf modules required
            tf_command = tf.Command('get')
            utils.run_command(tf_command, cwd=config['tf_root'])

            # validate env_name
            utils.validate_env_name(config['env_name'])
            tf_command = tf.Command('validate', config)
            utils.run_command(tf_command, cwd=config['tf_root'])
        return None
    
    def __getattr__(self, action):
        def default(*tf_opts, force = None):
            tf_cmd = None
            actions = {
                'apply'   : self._run_apply,
                'create'  : self._run_apply,
                'update'  : self._run_apply,
                'destroy' : self.destroy,
            }
            if actions.get(action):
                #tf_cmd = actions[action](action, force, *tf_opts)
                return actions[action](action, force, *tf_opts)
            
            return self._default(action, *tf_opts)

        return default

    def _env_exists(self):
        system_type = self.config['tags'].get('system_type', None)
        resources   = aws.environment_exists(self.env_name,
                                             self.env_version,
                                             system_type)
        
        return (system_type, resources)

    def _run_apply(self, cmd, force, *tf_opts):
        system_type = None
        if cmd in ['create', 'update']:
            (system_type, resources) = self._env_exists()
            resources_json = json.dumps(resources,indent=4)

        env_name = self.env_name
        if system_type:
            env_name = "-".join([system_type, self.env_name, self.env_version])

        if cmd == 'create' and len(resources) > 0:
            msg  = "\n\nAn environment with the name {} already exists."
            if not force:
                msg += "\nPlease tear it down before trying to rebuild."
                msg += "\n\n{}"
                msg = msg.format(env_name,resources_json)
                message = colored(msg, 'red')
                raise EnvironmentExistsException(message)
            else:
                msg += "\n{}\n\n--force detected..."
                msg += "\nCreating environment with the name {} anyway."
                msg += "\nYou may have resources with duplicate names."
                msg = msg.format(env_name,
                                 resources_json,
                                 env_name)
                message = colored(msg, 'red')
                logger.warning(message)

        if cmd == 'update' and len(resources) < 1:
            msg  = "\n\nNo such environment {} exists."
            msg += "\n\n{}"
            resources_json = json.dumps(resources,indent=4)
            message = colored(msg.format(self.env_name,resources_json), 'red')
            raise EnvironmentExistsException(message)
         
        try:
            # Run Apply
            tf_command = tf.Command('apply', self.config, *tf_opts)
            logger.debug("Command: {}".format(" ".join(tf_command)))
            logger.debug("In: {}".format(self.config['tf_root']))
            return_code = utils.run_command(tf_command,
                                            cwd=self.config['tf_root'])
        except:
            return False
        
        aws.tag_resources(self.config)
        return True

    def _default(self, cmd, *tf_opts):
        """
        Pass directly through to terraform
        """
        try:
            # Run Apply
            tf_command = tf.Command(cmd, self.config, *tf_opts)
            logger.debug("Command: {}".format(" ".join(tf_command)))
            logger.debug("In: {}".format(self.config['tf_root']))
            return_code = utils.run_command(tf_command, cwd=self.config['tf_root'])
        except:
            return False
        
    def query(self, *args):
        """
        Query AWS to see if an env already exists. If so, provide a list
        of ARNS.

        Returns:
            True or False

        """
        # Check if env already exists
        (system_type, resources) = self._env_exists()
        if len(resources) > 0:
            msg  = "{} exists."
            msg += "\n\n{}"
            resources_json = json.dumps(resources,indent=4)
            message = colored(msg.format(self.env_name,resources_json), 'red')
            print(message)

        return

    def destroy(self, cmd, *tf_opts):
        """
        Destroy the environment by running 'terraform destroy'.

        Args:
            config: dictionary containing all variable settings required
                    to run terraform with

        Returns:
            True

        Raises:
            EnvironmentExistsException if environment does not exist.
        """
        (system_type, resources) = self._env_exists()

        if len(resources) <= 0:
            msg = "No such environment with the name {} exists."
            if system_type:
                env_name = "-".join([system_type,
                                     self.env_name,
                                     self.env_version])
                logger.critical(msg)

        # Tag the resources as ready to destroy
        aws.tag_resources(self.config)

        # Run destroy
        #tf_command = tf.Command('destroy', self.config)
        tf_command = tf.Command('destroy', self.config, *tf_opts)
        return_code = utils.run_command(tf_command, cwd=self.config['tf_root'])

        # Double check the make sure we don't have anything left running
        # before destroying the S3 resources.
        env_exists = aws.environment_exists(self.env_name,
                                            self.env_version,
                                            system_type)
        if not env_exists and return_code == 0:
            # Destroy the per-environment S3 folder in
            folder = self.config['env_folder']
            msg = f"Destroying S3 env folder: {folder}"
            logger.debug(msg)
            s3.destroy_folder(self.config['project_config'],
                              self.config['env_folder'])

            # Destroy the state file in S3
            tf_state = self.config['tf_state']
            msg = f"Destroying S3 State file: {tf_state}"
            logger.debug(msg)
            s3.delete_object(self.config['tf_state_bucket'], self.config['tf_state'])
            return True
        
        # We didn't clean up properly
        return False

    def list_deployed_environment_versions(self):
        """
        Return a list of environemnt versions currently in use.

        Args:
            env: string name of a particular environement to check for
                 existing VPCs. e.g. dev, qa, prod, stage, etc.

        Returns:
            list of version letters in use.

        """
        vpcs = aws.list_vpcs(self.env_name)
        versions = [ version.split('-')[1] for version in aws.list_vpcs(self.env_name) ]
        versions.sort()
        return versions

    def get_next_version(self, env_name=None, ephemeral_env=None):
        """
        Return the next available version letter not currently in use.
        Will skip over letters in use. Will loop back to 'a' if it reaches
        'z'

        Args:
            env_name: string name of a particular environement to check for
                      existing VPCs. e.g. dev, qa, prod, stage, etc.

        ephemeral_env: Name of ephemeral environment based on system_type tag.

        Returns:
            character representing next available, unused environment version.

        """
        if not self.env_name:
            if not env_name:
                logger.critical("env_name not set!")
                sys.exit(1)
            self.env_name = env_name
            
        next_env = 'a'
        while aws.environment_exists(self.env_name,next_env,ephemeral_env):
            # increment to next version.
            # If we're already at 'z', loop back to 'a'
            if ord(next_env.lower()) == 122:
                next_env = 'a'
            else:
                # otherwise increment
                next_env = chr(ord(next_env.lower()) + 1)
        return next_env

