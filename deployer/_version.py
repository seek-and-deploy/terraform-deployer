import datetime
import os


def _get_version():
    date = datetime.datetime.now().strftime("%Y%m%d")
    filepath = os.path.join(os.path.dirname(__file__), ".version")
    version = 0
    if os.path.exists(filepath) and os.path.getsize(filepath) != 0:
        with open(filepath, 'r') as version_file:
            version = int(version_file.read())

    version += 1
    pkg_version = "{}.{}".format(date,version)

    with open(filepath, 'w') as version_file:
        version_file.write(str(version))
    return pkg_version


__version__ = _get_version()
__version_info__ = '.'.split(__version__)
