# -*- coding: utf-8 -*-
#
# Copyright Veracode Inc., 2014
import logging
import os

logger = logging.getLogger(os.path.basename('deployer'))


class Terraform(object):

    def __init__(self):
        return None

    def _init(self, config=None):
        """
        Generate command for 'terraform remote config ...'.

        Args:
            config:   Dictionary containing all variable settings required
                      to run terraform with.

            tf_opts:  List of additional terraform options to run with.

        Returns:
            tf_command: list of command-line arguments to run terraform with.
        """
        tf_cmd = ['terraform',
                  'init',
                  '-backend=true',
                  f"-backend-config=bucket={config['tf_state_bucket']}",
                  f"-backend-config=key={config['tf_state']}",
                  f"-backend-config=region={config['aws_region']}",
                  f"-backend-config=profile={config['aws_profile']}"]

        return tf_cmd

    def _auto_approve_cmds(self, cmd, config):
        """
        Generate command for 'terraform [ apply | destroy ]' or any others
        which need default cmd line options + '-auto-approve'

        Args:
            cmd:    The command we need to construct
            config: dictionary containing all variable settings required
                    to run terraform with

        Returns:
            tf_command: list of command-line arguments to run terraform with.
        """
        tf_cmd = ['terraform', cmd, '-auto-approve']
        tf_cmd += default_cmdline_options(config)
        return tf_cmd

    def _standard_cmds(self, cmd, config):
        """
        Generate command for 'terraform [ plan | refresh | import ]' or any
        others we need to add default options to.

        Args:
            cmd:    The command we need to construct
            config: dictionary containing all variable settings required
                    to run terraform with

        Returns:
            tf_command: list of command-line arguments to run terraform with.
        """
        tf_cmd = ['terraform', cmd]
        tf_cmd += default_cmdline_options(config)

        return tf_cmd

    def _default(self, cmd, config=None):
        """
        Generate the command line for 'terraform foo <extra options>'.

        Args:
            config:   Dictionary containing all variable settings required
                      to run terraform with.
            cmd:      The terraform command to run.
            tf_opts:  List of additional terraform options to run with.

        Returns:
            tf_command: list of command-line arguments to run terraform with.
        """
        tf_command = ['terraform', cmd ]

        return tf_command

    def Command(self, cmd, config=None, tf_opts=None):
        commands = {
            'apply'   : self._auto_approve_cmds,
            'destroy' : self._auto_approve_cmds,
            'import'  : self._standard_cmds,
            'plan'    : self._standard_cmds,
            'refresh' : self._standard_cmds }

        try:
            if commands.get(cmd):
                tf_cmd = commands[cmd](cmd, config)
            else:
                tf_cmd = getattr(self, f"_{cmd}")(config)
        except AttributeError:
            tf_cmd = self._default(cmd, config)

        if tf_opts:
            tf_cmd += tf_opts

        return tf_cmd


def default_cmdline_options(config):
    """
    Return default command line options required for every run of terraform.

    Args:
        config: dictionary containing all variable settings required
                to run terraform with

    Returns:
        cmdline_options: list of command-line arguments to run
                         all terraform commands should be run with.
    """

    cmdline_options = ['-var=aws_region={}'.format(config['aws_region']),
                       '-var-file={}'.format(config['tfvars']) ]

    return cmdline_options
