# -*- coding: utf-8 -*-
#
# Copyright Veracode Inc., 2014

from jinja2 import Environment
import deep_merge
import json
from   jsonschema import validate
from   jsonschema.exceptions import ValidationError
import logging
import os
from   subprocess import (STDOUT,
                          PIPE,
                          Popen)
import sys
from termcolor import colored
from ruamel import yaml
from deployer import (__version__,
                      __appname__)
from deployer.exceptions import EnvironmentNameException


logger = logging.getLogger('deployer')
#logger.propagate = False


def load_vars(varfile):
    """
    Load a specified variables file.

    Args:
        varfile: string (file path) location of the deployer config file.

    Returns:
        data: dict. This becomes the 'config' dict that gets passed around.
    """
    with open(varfile) as data_file:
        try:
            #data = json.load(data_file)
            data = yaml.safe_load(data_file)
        except yaml.scanner.ScannerError as tabs:
            #except json.JSONDecodeError as tabs:
            msg = "Found illegal tabs in file at line: {0}, character: {1}"
            logger.critical(msg.format(tabs.problem_mark.line,
                                       tabs.problem_mark.column))
            sys.exit()

    return data


def merge_vars(varfiles):
    """
    Process a list of files containing a json dict and merge them into
    a single file for output.


    Args:
        varfiles: list of file paths.

    Returns:
        dict containing the merged contents of the list of files passed in.
    """

    config = {}
    for path in varfiles:
        config_template_path = os.path.abspath(path)
        config_template = load_vars(config_template_path)
        config = deep_merge.merge(config, config_template)

    return config


def eval_config(config):

    """
    Use the passed config dict as a template and return the fully
    evaluated config back to the caller.

    Args:
        config: dict containing a deployer configuration

    Returns:
        data: dict. This becomes the 'config' dict that gets passed around/
    """
    # Convert the dict back to a string, eval the tempplate, then
    # convert it back to a dict.
    config_str = json.dumps(config)
    env = Environment()
    template = env.from_string(config_str)
    data = json.loads(template.render(config = config))
    data2 = json.loads(template.render(config = data))

    return data2


def load_schema(schema):
    """
    Load a specified variables file.

    Args:
        schema: string (file path) location of a schema file to use
                in validations.

    Returns:
        dict via a call to load_vars()
    """
    f = os.path.join(os.path.dirname(__file__), schema)
    return load_vars(f)


def validate_env_name(env_name):
    """
    Ensure environment name, once constructed from:
      environment['name'] + environment['version']

    follows various rules and conventions, both local, and imposed by AWS.

    Args:
        env_name: string representing an environment name

    Returns:
        True on success

    Raises:
      EnvironmentNameException on failure.

    """

    if len(env_name) <= 0:
        msg = "Incorrect number of tokens. "
        msg += "Environment name can not be undefined"
        raise EnvironmentNameException(msg)

    if len(env_name) > 20:
        msg = "Environment Name too long: {} characters. Limit: 20."
        raise EnvironmentNameException(msg.format(len(env_name)))

    # environment name must be at least 1 token
    # Single token environment names are typically shared/master environments
    # Double token environment names are typically ephemeral environments
    # Triple+ token environment names are typically multi-vpc environments
    # where we have a clique of inter-related pods.
    parsed_name = []
    parsed_name = env_name.split('-')
#    import pdb
#    pdb.set_trace()
    if (len(parsed_name) > 1):
        msg  = "Environment version is limited to a single character "
        msg += "or an integer: {}"
        if parsed_name[-1].isalpha():
            if (len(parsed_name[-1]) != 1):
                raise EnvironmentNameException(msg.format(parsed_name[-1]))
        elif not (parsed_name[-1].isnumeric() and parsed_name[-1].isdigit()):
            raise EnvironmentNameException(msg.format(parsed_name[-1]))

    return True


def validate_schema(config, schema_file):
    """
    Validate the config based on a defined schema.

    Args:
        config: dictionary containing all variable settings required
                to run terraform with

        schema_file: string (file path) location of a schema file to use
                     to validate the config dict.

    Returns:
         True

    Raises:
         ValidationError on failure.
    """
    try:
        schema = load_schema(schema_file)
        validate(config, schema)
    except ValidationError as e:
        msg = e.message + "\n"
        version_errors = [ "does not match '^[a-z]$'",
                           "does not match '^[0-9]+$'",
                           "does not match 'GET_NEXT'" ]

        for error in version_errors:
            if error in e.message:
                msg += "env:version must be one of: single alpha character, "
                msg += "integer, or the string 'GET_NEXT'"

        if "does not match '^[a-z0-9]{1,10}$'" in  e.message:
            msg += "env:name is invalid. It must contain only alpha-numeric "
            msg += "characters, and be between 1 and 10 characters."

        if "is not of type 'string'" in e.message:
            msg += "env:version must be an unquoted integer, not a real number"

        if "is a required property" in e.message:
            msg += " "

        # Handle any of the above errors.
        if len(msg) > len(e.message + "\n"):
            logger.critical(msg)
            sys.exit(msg)

        # fall back to raising the standard validation errors for
        # anything not handled above.
        raise

    return True


def run_command(command, cwd=None):
    """
    Runs a command "on the command line" and returns stdout/stderr in
    real time.

    Args:
        command: list of command line arguments to run.
        cwd    : string (path) representing the location of where to run the
                 command from.
    Returns:
        out: out put of the command run.
    """
    logger.debug("{}: Running command: '{}' in {}".format(__name__,
                                                          " ".join(command),
                                                          cwd ))
    cmd = Popen(command, shell=False, bufsize=0, stderr=STDOUT, cwd=cwd)
    (out, err) = cmd.communicate()
    if err:
        sys.exit(err)
        print(out)

    if cmd.returncode != 0:
        msg = f"{command} failed with code {cmd.returncode}."
        logger.error(msg)
        sys.exit(msg)

    return cmd.returncode


def return_command_output(command, cwd=None):
    """
    Runs a command "on the command line" and returns stdout/stderr to the calling
    function.

    Args:
        command: list of command line arguments to run.
        cwd    : string (path) representing the location of where to run the
                 command from.
    Returns:
        out: string containing the output of the command run
    """
    logger.debug("{}: Running command: '{}' in {}".format(__name__,
                                                          " ".join(command),
                                                          cwd ))
    cmd = Popen(command,
                shell=False,
                bufsize=0,
                stdout=PIPE,
                stderr=STDOUT,
                cwd=cwd,
                universal_newlines=True)

    (output, err) = cmd.communicate()
    if err:
        logger.error(output)
        sys.exit(err)

    if cmd.returncode != 0:
        err_msg = f"{command} failed with code {cmd.returncode}."
        logger.error(colored(err_msg, 'white', attrs=['bold']))
        sys.exit(f"{output}")

    return output


def git_clone(repo, branch=None):
    """
    Return the git command to clone a specified repository.

    Args:
        repo: string representing a git repo.
        branch: string representing a branch name.

    Returns:
        cmd: list containing the command line arguments to clone a git repo.
    """
    cmd = ['git', 'clone', '--recursive']
    if branch:
        cmd += ['-b', branch]
    cmd.append(repo)

    return cmd


def git_pull(repo):
    """
    Return the git command to update an existing repo

    Args:
        repo: string representing a git repo.

    Returns:
        cmd: list containing the command line arguments to clone a git repo.
    """
    cmd = ['git', 'pull']

    return cmd


def git_set_branch(branch):
    """
    Change to a given branch in a local copy of the checked out repository

    Args:
        branch: string representing the branch to change to.

    Returns:
        cmd: list containing the command line arguments to set a git repo to
    """

    return ['git', 'checkout', branch]


def parse_git_url(url):
    """
    Parse the provided git URL into components to operate on independently.

    Args:
        url: string representing a gitlab url for a git repo.

    Returns:
        repo, branch: tuple of strings representing a git repo and a
                      branch name.
    """
    repo = None
    branch = None
    subdir = None
    if 'http' in url:
        (repo, branch, subdir) = parse_http_git_url(url)
    elif "?" in url:
        (repo,branch) = url.split('?')
        branch = branch.split('=')[1]
        if '//' in branch:
            (branch,subdir) = branch.split('//')
    elif '//' in url:
        (repo, subdir) = url.split('//')
    else:
        repo = url
    return (repo, branch, subdir)


def parse_http_git_url(url):
    repo = None
    branch = None
    subdir = None

    if "?" in url:
        (repo,branch) = url.split('?')
        branch = branch.split('=')[1]
        if '//' in branch:
            (branch,subdir) = branch.split('//')
    elif len(url.split('//')) > 2:
        subdir = url.split('//')[-1]
        repo = '//'.join(url.split('//')[0:2])
    else:
        repo = url

    return (repo, branch, subdir)


def git_url(url):
    return url.endswith('.git') or '.git' in url


def local_dir_from_git_repo(url):
    """
    Determine the name of the directory we checked out based on the URL.

    Args:
        url: string representng the git url for the terraform code repo.
    Returns:
        project: string represeting just the project name, stripped of '.git'.
    """
    repo = url
    branch = None
    subdir = None
    if '?' in url or '//' in url:
        (repo,branch,subdir) = parse_git_url(url)

    repo_name = repo.split('/')[-1]
    project = repo_name.replace('.git','')
    if subdir:
        project = os.path.join(project, subdir)

    return project


def get_tf_location(config):
    """
    Return the location of where terraform lives locally by parsing
    the config['terraform'] value.

    Args:
        config: dictionary containing all variable settings required
                to run terraform with

    Returns:
        Local path indicating where the terraform infrastructure code
        lives on the "local disk"
    """

    if config.get('terraform'):
        # If tf code is in a git repo, check it out under config['tmp_dir']
        if git_url(config['terraform']):
            project = local_dir_from_git_repo(config['terraform'])
            tf_root = config.get('tf_root',
                                 os.path.join(config['tmpdir'], project))

        # If we've provided a specific location, just use that location.
        if not git_url(config['terraform']):
            tf_root = config['terraform']

        return tf_root


def parseConfigOpts(configVars):
    newConfigVars = {}
    for item in configVars:
        try:
            value = json.loads(item)
            newConfigVars = { **newConfigVars, **value }
        except ValueError as e:
            k,v = item.split('=')
            newConfigVars[k] = v

    return newConfigVars


def add_version_tags(tags):
    local_tags = tags
    local_tags[ __appname__ ] = __version__

    command = ["terraform", "-v"]
    return_val = return_command_output(command)

    local_tags["terraform"] = return_val.splitlines()[0].split(" ")[1]
    return local_tags
