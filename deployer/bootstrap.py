# -*- coding: utf-8 -*-
#
# Copyright Veracode Inc., 2014

import logging
import os

from   deployer import s3


logger = logging.getLogger(os.path.basename('deployer'))


def bootstrap(config):
    """
    Configure the existing account for subsequent deployer runs.
    Create S3 buckets & folders.

    Args:
        config: dictionary containing all variable settings required
                to run terraform with

    Returns:
        config dict.
    """
    config['project_config'] = config.get('project_config',
                                          s3.get_bucket_name(config, 'data'))
    config['tf_state_bucket'] = config.get('tf_state_bucket',
                                           s3.get_bucket_name(config,'tfstate'))

    logmsg = "{}: Creating S3 project bucket: {}"
    logger.debug(logmsg.format(__name__, config['project_config']))
    s3.create_bucket(config['project_config'], config['aws_region'])

    logmsg = "{}: Creating S3 project bucket: {}"
    logger.debug(logmsg.format(__name__, config['tf_state_bucket']))
    s3.create_bucket(config['tf_state_bucket'], config['aws_region'])

    return config


