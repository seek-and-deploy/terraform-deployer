from    docopt    import docopt
#import  importlib
import  logging
import  os
import  sys
from    termcolor import colored

from    deployer  import __version_string__
from    deployer  import utils

logger = logging.getLogger(os.path.basename('deployer'))


def parseArgs(argv, docs):
    tf_args = None
    try:
        tf_args_start = argv.index('--')
        if tf_args_start > 0:
            tf_args = argv[tf_args_start + 1 :]
            sys.argv = argv[0:tf_args_start]
    except:
        pass

    my_args = docopt(docs,
                     version=__version_string__,
                     options_first=False)

    if tf_args:
        my_args['<tf_options>'] = tf_args
        argv = [my_args['<cmd>']] + my_args['<tf_options>']
    if my_args.get('help'):
        # if my_args.get('<cmd>') in ['create', 'update', 'query']:
        #     cmd    = my_args['<cmd>']
        #     mod    = f"deployer.deployer_{cmd}"
        #     module = importlib.import_module(mod)
        #     print(module.__doc__)
        # else:
        argv = ['terraform'] + argv + ['-h']
        utils.run_command( argv )
        sys.exit()

    bootstrap_cmds = ['apply', 'create', 'plan']
    if my_args.get('--bootstrap') and my_args.get('<cmd>') not in bootstrap_cmds:
        print(docs)
        msg = "ERROR: --bootstrap is only compatible with 'apply', 'create', and 'plan'."
        print(colored(msg, 'red'))
        sys.exit(1)

    return my_args
