'''
Unit tests for the utils.py module. Currently we do not test the
validity of anything dependent upon boto3 calls, since we haven't
figured out how to mock those calls out with moto properly.  And,
apparently, the features of boto3 we use have not yet been implemented
in moto yet.
'''

import deep_merge
import json
from   mock import patch
import os
import pytest
import workdir
import yaml

import deployer.utils as utils
from   deployer.exceptions import EnvironmentNameException

top_dir = os.path.join(os.path.dirname(__file__),
                       os.pardir,
                       os.pardir)
deployer_dir  = os.path.join(top_dir, 'deployer')
conf_dir      = os.path.join(deployer_dir, 'conf')
test_dir      = os.path.join(top_dir, 'tests')
test_data_dir = os.path.join(test_dir, 'test_data')


@pytest.fixture
def setup_teardown(scope="function"):
    # Make sure test environment doesn't exist
    mock_config_file = os.path.join(test_data_dir, "mock_config.json")
    with open(mock_config_file, 'r') as mock:
        mock_config = json.load(mock)

    tmpdir       = '/tmp/test_tmp_dir'
    tmpfile      = os.path.join(tmpdir, "test_vars.json")
    tmpfile_yaml = os.path.join(tmpdir, "test_vars_yaml.json")

    workdir.options.path = tmpdir
    workdir.create()
    with open(tmpfile, 'w') as tfile:
        json.dump(mock_config, tfile, indent=2)

    with open(tmpfile_yaml, 'w') as tfile:
        yaml.dump(mock_config, tfile)

    yield {
        'mock_config' : mock_config,
        'filename' : tmpfile,
        'filename_yaml' : tmpfile_yaml
    }
    # Make sure we clean up if anything else actually creates it
    workdir.options.path = tmpdir
    workdir.remove()


def test_load_vars(setup_teardown):
    loaded_data = utils.load_vars(setup_teardown['filename'])

    assert loaded_data == setup_teardown['mock_config']


def test_load_vars_yaml(setup_teardown):
    loaded_yaml = utils.load_vars(setup_teardown['filename_yaml'])
    assert loaded_yaml == setup_teardown['mock_config']


def test_validate_env_name_num_tokens():
    shared_env_name = 'env'
    ephemeral_env_name = 'env-a'
    too_few_tokens = ''

    assert utils.validate_env_name(shared_env_name)
    assert utils.validate_env_name(ephemeral_env_name)

    except_msg = "Incorrect number of tokens. "
    except_msg += "Environment name can not be undefined"
    with pytest.raises(EnvironmentNameException) as e:
        utils.validate_env_name(too_few_tokens)
    assert (e.value.args[0] == except_msg)


def test_validate_env_name_length():
    good_name = 'thisisalongenvname-a'
    bad_name = 'thisenvnameistoolong-a'

    assert utils.validate_env_name(good_name)

    except_msg = "Environment Name too long: {} characters. Limit: 20."
    with pytest.raises(EnvironmentNameException) as e:
        utils.validate_env_name(bad_name)
    assert (e.value.args[0] == (except_msg.format(len(bad_name))))


def test_validate_env_name_version_is_char():
    good_name = 'env-a'
    bad_name_double_char = 'env-aa'
    
    assert utils.validate_env_name(good_name)

    except_msg  = "Environment version is limited to a single character "
    except_msg += "or an integer: {}"
    msg_double_char = except_msg.format(bad_name_double_char.split('-')[-1])
    
    with pytest.raises(EnvironmentNameException) as e:
        utils.validate_env_name(bad_name_double_char)
    assert (e.value.args[0] == msg_double_char)


@pytest.fixture
def basic_schema(scope="function"):
    path = os.path.join(conf_dir, 'default_schema.json')
    return path


@pytest.fixture
def terraform_schema(scope="function"):
    path = os.path.join(conf_dir, 'terraform_schema.json')
    return path


def test_validate_schema_good(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_complete_config.json')

    basic_config = utils.load_vars(basic_complete_config)
    utils.validate_schema(basic_config, basic_schema_file)


def test_validate_schema_GET_NEXT(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_with_GET_NEXT_version.json')

    basic_config = utils.load_vars(basic_complete_config)
    utils.validate_schema(basic_config, basic_schema_file)


def test_validate_schema_bad_GET_NEXT(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_with_bad_GET_NEXT_version.json')
    with pytest.raises(SystemExit) as e:
        basic_config = utils.load_vars(basic_complete_config)
        utils.validate_schema(basic_config, basic_schema_file)

    assert "'GET_NEXT123' does not match '^[a-z]$'" in e.value.args[0]


def test_validate_schema_bad_env_name_w_dash(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_bad_env_name.json')

    with pytest.raises(SystemExit) as e:
        basic_config = utils.load_vars(basic_complete_config)
        utils.validate_schema(basic_config, basic_schema_file)

    assert "'my-envname' does not match '^[a-z0-9]{1,10}$'" in e.value.args[0]


def test_validate_schema_bad_env_name_w_num(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_bad_env_name_w_number.json')

    with pytest.raises(SystemExit) as e:
        basic_config = utils.load_vars(basic_complete_config)
        utils.validate_schema(basic_config, basic_schema_file)

    assert "12 is not of type 'string'" in e.value.args[0]


def test_validate_schema_env_vers_w_num(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_bad_env_vers_w_number.json')
    basic_config = utils.load_vars(basic_complete_config)
    utils.validate_schema(basic_config, basic_schema_file)


def test_validate_schema_bad_env_vers_too_long(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_bad_env_vers_too_long.json')

    with pytest.raises(SystemExit) as e:
        basic_config = utils.load_vars(basic_complete_config)
        utils.validate_schema(basic_config, basic_schema_file)

    assert "'aaa' does not match '^[a-z]$'" in e.value.args[0]


def test_validate_schema_int_version(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_with_int_version.json')

    basic_config = utils.load_vars(basic_complete_config)
    utils.validate_schema(basic_config, basic_schema_file)


def test_validate_schema_int_letter_version(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_with_int_letter_version.json')

    with pytest.raises(SystemExit) as e:
        basic_config = utils.load_vars(basic_complete_config)
        utils.validate_schema(basic_config, basic_schema_file)
    assert "'1a' does not match '^[a-z]$'" in e.value.args[0]


def test_validate_schema_letter_int_version(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_with_letter_int_version.json')

    with pytest.raises(SystemExit) as e:
        basic_config = utils.load_vars(basic_complete_config)
        utils.validate_schema(basic_config, basic_schema_file)
    assert "'a1' does not match '^[a-z]$'" in e.value.args[0]


def test_validate_schema_long_int_version(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_with_long_int_version.json')

    basic_config = utils.load_vars(basic_complete_config)
    utils.validate_schema(basic_config, basic_schema_file)


def test_validate_schema_int_as_string_version(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_with_int_as_string_version.json')

    basic_config = utils.load_vars(basic_complete_config)
    return_val = utils.validate_schema(basic_config, basic_schema_file)

    assert return_val


def test_validate_schema_realnum_version(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_with_realnum_version.json')

    with pytest.raises(SystemExit) as e:
        basic_config = utils.load_vars(basic_complete_config)
        utils.validate_schema(basic_config, basic_schema_file)
    assert "1.2 is not of type 'string'" in e.value.args[0]


def test_validate_schema_realnum_as_string_version(basic_schema):
    basic_schema_file = basic_schema
    basic_complete_config = os.path.join(test_data_dir,
                                         'base_config_with_realnum_as_string_version.json')
    with pytest.raises(SystemExit) as e:
        basic_config = utils.load_vars(basic_complete_config)
        utils.validate_schema(basic_config, basic_schema_file)

    assert "'1.2' does not match '^[a-z]$'" in e.value.args[0]


def test_validate_schema_missing_required_property(basic_schema):
    basic_schema_file = basic_schema
    with pytest.raises(SystemExit) as e:
        basic_incomplete_config = os.path.join(test_data_dir,
                                               'base_incomplete_config.json')
        basic_config = utils.load_vars(basic_incomplete_config)
        utils.validate_schema(basic_config, basic_schema_file)

    assert "'aws_profile' is a required property" in e.value.args[0]


def test_validate_tf_schema_good(terraform_schema):
    tf_schema_file = terraform_schema
    tf_complete_config = os.path.join(test_data_dir,
                                      'terraform_complete_data.json')
    tf_config = utils.load_vars(tf_complete_config)
    utils.validate_schema(tf_config, tf_schema_file)


def test_validate_tf_schema_missing_required_property(terraform_schema):
    tf_schema_file = terraform_schema
    with pytest.raises(SystemExit) as e:
        tf_incomplete_config = os.path.join(test_data_dir,
                                            'terraform_incomplete_data.json')
        tf_config = utils.load_vars(tf_incomplete_config)
        utils.validate_schema(tf_config, tf_schema_file)
    assert "'public_zone_id' is a required property" in e.value.args[0]


def test_git_pull():
    repo = "/some/local/path"
    expected_cmd = ['git', 'pull']
    returned_cmd = utils.git_pull(repo)
    assert returned_cmd == expected_cmd


def test_git_clone_with_branch():
    repo = 'git@gitlab.org:group/project'
    branch = "made_up_branch"
    expected_cmd = ['git',
                    'clone',
                    '--recursive',
                    '-b',
                    branch,
                    repo]

    return_cmd = utils.git_clone(repo, branch)
    assert expected_cmd == return_cmd


def test_git_clone_without_branch():
    repo = 'git@gitlab.org:group/project'
    expected_cmd = ['git',
                    'clone',
                    '--recursive',
                    repo]

    return_cmd = utils.git_clone(repo)
    assert expected_cmd == return_cmd


def test_git_url_with_nongit_url():
    url = "/some/local/path"
    assert not utils.git_url(url)

    expected_repo = '/some/local/path'
    expected_branch = None
    expected_subdir = None
    (repo, branch, subdir) = utils.parse_git_url(url)


def test_git_url_with_nongit_url_with_branch():
    url = "/some/local/path?branch=BRANCH_NAME"
    assert not utils.git_url(url)

    expected_repo = '/some/local/path'
    expected_branch = 'BRANCH_NAME'
    expected_subdir = None
    (repo, branch, subdir) = utils.parse_git_url(url)


def test_git_url_with_nongit_url_with_subdir():
    url = "/some/local/path//subdir"
    assert not utils.git_url(url)

    expected_branch = None
    expected_subdir = 'subdir'
    (repo, branch, subdir) = utils.parse_git_url(url)


def test_git_url_with_nongit_url_with_branch_and_subdir():
    url = "/some/local/path?branch=BRANCH_NAME//subdir"
    assert not utils.git_url(url)

    expected_repo = '/some/local/path'
    expected_branch = 'BRANCH_NAME'
    expected_subdir = 'subdir'
    (repo, branch, subdir) = utils.parse_git_url(url)


def test_git_url_with_nongit_url_with_dotgit():
    # should fail
    url = "/some/local/path.git"
    #assert not utils.git_url(url)

    url = "/some/local/path?branch=BRANCH_NAME//subdir"

    expected_repo = '/some/local/path'
    expected_branch = 'BRANCH_NAME'
    expected_subdir = 'subdir'
    (repo, branch, subdir) = utils.parse_git_url(url)


def test_git_url_with_no_subdir_no_branch():
    url = 'git@gitlab.org:group/project.git'
    assert utils.git_url(url)

    expected_repo = 'git@gitlab.org:group/project.git'
    expected_branch = None
    expected_subdir = None
    (repo, branch, subdir) = utils.parse_git_url(url)

    assert expected_repo == repo
    assert expected_branch == branch
    assert expected_subdir == subdir


def test_git_url_with_branch_no_subdir():
    url = 'git@gitlab.org:group/project.git?branch=made_up_branch'
    assert utils.git_url(url)

    expected_repo = 'git@gitlab.org:group/project.git'
    expected_branch = 'made_up_branch'
    expected_subdir = None
    (repo, branch, subdir) = utils.parse_git_url(url)

    assert expected_repo == repo
    assert expected_branch == branch
    assert expected_subdir == subdir


def test_git_url_with_subdir_no_branch():
    url = 'git@gitlab.org:group/project.git//subdir'
    assert utils.git_url(url)

    expected_repo = 'git@gitlab.org:group/project.git'
    expected_branch = None
    expected_subdir = 'subdir'

    (repo, branch, subdir) = utils.parse_git_url(url)

    assert expected_repo == repo
    assert expected_branch == branch
    assert expected_subdir == subdir


def test_git_http_url_with_no_branch_no_subdir():
    url = 'https://git@gitlab.org:group/project.git'
    assert utils.git_url(url)

    expected_repo = 'https://git@gitlab.org:group/project.git'
    expected_branch = None
    expected_subdir = None

    (repo, branch, subdir) = utils.parse_git_url(url)

    assert expected_repo == repo
    assert expected_branch == branch
    assert expected_subdir == subdir


def test_git_http_url_with_branch_no_subdir():
    url = 'https://git@gitlab.org:group/project.git?branch=made_up_branch'
    assert utils.git_url(url)

    expected_repo = 'https://git@gitlab.org:group/project.git'
    expected_branch = 'made_up_branch'
    expected_subdir = None
    (repo, branch, subdir) = utils.parse_git_url(url)

    assert expected_repo == repo
    assert expected_branch == branch
    assert expected_subdir == subdir


def test_git_http_url_with_subdir_no_branch():
    url = 'https://git@gitlab.org:group/project.git//subdir'
    assert utils.git_url(url)

    expected_repo = 'https://git@gitlab.org:group/project.git'
    expected_branch = None
    expected_subdir = 'subdir'
    (repo, branch, subdir) = utils.parse_git_url(url)

    assert expected_repo == repo
    assert expected_branch == branch
    assert expected_subdir == subdir


def ttest_git_http_url_with_branch_and_subdir():
    url = 'git@gitlab.org:group/project.git?branch=made_up_branch//subdir'
    expected_repo = 'git@gitlab.org:group/project.git'
    expected_branch = 'made_up_branch'
    expected_subdir = 'subdir'
    (repo, branch, subdir) = utils.parse_git_url(url)

    assert expected_repo == repo
    assert expected_branch == branch
    assert expected_subdir == subdir

    return


def test_local_dir_from_repo_full_url():
    url = 'git@gitlab.org:group/project.git?branch=made_up_branch//subdir'
    expected_dir = 'project/subdir'
    returned_dir = utils.local_dir_from_git_repo(url)

    assert expected_dir == returned_dir


def test_local_dir_from_repo_just_repo():
    repo = 'git@gitlab.org:group/project.git'
    expected_dir = 'project'
    returned_dir = utils.local_dir_from_git_repo(repo)
    assert expected_dir == returned_dir


def test_get_tf_location_from_local_path():
    mock_config = { "terraform": "/some/local/path" }
    expected_root = "/some/local/path"
    returned_root = utils.get_tf_location(mock_config)

    assert expected_root == returned_root


def test_get_tf_location_from_git_url():
    mock_config = {
        "tmpdir" : '/tmp/test_tmp_dir',
        "terraform": "git@gitlab.org:group/project.git?branch=made_up_branch"
    }
    expected_root = os.path.join( mock_config['tmpdir'], "project" )
    returned_root = utils.get_tf_location(mock_config)

    assert expected_root == returned_root


def test_parseConfigOpts():

    mock_args = [ '{ "tmpdir" : "/fizzle/drizzle" }',
                  '{ "environment" : { "name" : "coral", "version" : "q"} }',
                  '{ "random_list" : [ 0, 1, 2, 3, 4, 5 ] }' ]

    expected = { "tmpdir" : "/fizzle/drizzle",
                 "environment" : { "name" : "coral",
                                   "version" : "q"},
                 "random_list" : [ 0, 1, 2, 3, 4, 5 ] }

    return_stuff = utils.parseConfigOpts(mock_args)
    assert return_stuff == expected


def test_git_set_branch():
    url = "/some/local/path?branch=BRANCH_NAME"

    assert not utils.git_url(url)
    expected_cmd = [ 'git', 'checkout', 'BRANCH_NAME' ]

    (repo, branch, subdir) = utils.parse_git_url(url)
    returnd_cmd = utils.git_set_branch(branch)

    assert expected_cmd == returnd_cmd


def test_run_command():

    import platform
    systype = platform.system()

    truefalse = {
        'Darwin' : '/usr/bin',
        'Linux' : '/bin'
    }

    command = [ "{}/true".format(truefalse[systype]) ]
    # pytest.set_trace()
    expected_code = 0
    return_code = utils.run_command(command, cwd = '/tmp')
    assert return_code == expected_code

    command = [ "{}/false".format(truefalse[systype]) ]
    #pytest.set_trace()
    expected_code = 1
    with pytest.raises(SystemExit) as e:
        return_code = utils.run_command(command, cwd = '/tmp')

    assert e.typename == 'SystemExit'
    assert f"['{truefalse[systype]}/false'] failed with code 1." == e.value.code


def test_return_command_output():

    import platform
    systype = platform.system()

    truefalse = {
        'Darwin' : '/bin',
        'Linux'  : '/bin'
    }

    command = [ "{}/date".format(truefalse[systype]) ]
    return_val = utils.return_command_output(command, cwd = '/tmp')

    import re
    parsed_return = re.split(' +', return_val)
    assert len(parsed_return) == 6

    time = parsed_return[3].split(":")
    assert len(time) == 3


def test_merge_vars():
    common_vars_file = os.path.join(test_data_dir, "app_vars_data.json")
    with open(common_vars_file , 'r') as cvars:
        common_vars = json.load(cvars)

    app_vars_file    = os.path.join(test_data_dir, "common_vars_data.json")
    with open(app_vars_file , 'r') as appvars:
        app_vars = json.load(appvars)

    expected_merge_file = os.path.join(test_data_dir,
                                       "expected_merged_vars.json")
    with open(expected_merge_file , 'r') as emerge:
        expected_merge_vars = json.load(emerge)

    merged_vars = deep_merge.merge(common_vars, app_vars)
    assert merged_vars == expected_merge_vars


def test_merge_vars_w_override():
    common_vars_file = os.path.join(test_data_dir, "app_vars_data.json")
    with open(common_vars_file , 'r') as cvars:
        common_vars = json.load(cvars)

    app_vars_file    = os.path.join(test_data_dir, "common_vars_data.json")
    with open(app_vars_file , 'r') as appvars:
        app_vars = json.load(appvars)

    expected_merge_file = os.path.join(test_data_dir,
                                       "expected_merged_vars_w_overrides.json")
    with open(expected_merge_file , 'r') as emerge:
        expected_merge_vars = json.load(emerge)

    override_a = { "tmpdir": "/tmp/foo"}
    override_b = { "tags" : { "foo" : "bar"}}

    import deep_merge
    merged_vars = deep_merge.merge(common_vars, app_vars)
    merged_vars = deep_merge.merge(merged_vars, override_a, override_b)
    assert merged_vars == expected_merge_vars


def mock_return_output(args, cwd=None):
    print("CWD: {}, Running command: {}".format(cwd, " ".join(args)))
    return """Terraform v0.14.4

    Your version of Terraform is out of date! The latest version
    is 0.15.0. You can update by downloading from https://www.terraform.io/downloads.html
    """


def test_add_version_tags():
    tags = {
        "foo" : "bar",
        "a"   : "b",
        "Fred" : "Barney"
    }
    from deployer import __version__
    expected_tags = tags
    expected_tags['seek-and-deploy'] = __version__
    expected_tags['terraform'] = 'v0.14.4'

    with patch('deployer.utils.return_command_output', mock_return_output):
        returned_tags = utils.add_version_tags(tags)

    assert expected_tags == returned_tags
