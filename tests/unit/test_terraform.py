'''
Unit tests for the terraform.py module. Currently we do not test
actual calls to terraform, just the various utility methods in
terraform.py which are testable.

'''
import pytest
from deployer.terraform import Terraform

tf = Terraform()


@pytest.fixture
def mock_config(scope="function"):
    return {
        "aws_profile" : "my_aws_profile",
        "aws_region" : "us-east-1",
        "availability_zones"  : [
            'us-east-1b',
            'us-east-1c',
            'us-east-1d',
            'us-east-1e'
        ],
        "account_id" : "123456789012",
        "environment" : {
            "name" : "myenvname",
            "version" : "a",
        },
        "env_name" : "myenvname-a",
        "tf_state_bucket" : "123456789012-myproj-tfstate",
        "tf_state" : "myenvname-a.tfstate",
        "project_config" : "123456789012-myproj-data",
        "project" : 'myproj',
        "tfvars" : '/tmp/test_tmp_dir/vars.tf'
    }


def test_tf_get_no_opts():
    expected = ['terraform', 'get']
    result = tf.Command('get')
    assert expected == result
    return


def test_tf_get_with_1_opt(mock_config):
    tf_opts = ['-update']
    expected = ['terraform', 'get'] + tf_opts
    result = tf.Command('get', config=None, tf_opts=tf_opts)
    assert expected == result
    return


def test_tf_get_with_2_opts():
    tf_opts = ['-update', '-no-color']
    expected = ['terraform', 'get' ] + tf_opts
    result = tf.Command('get', config=None, tf_opts=tf_opts)
    assert expected == result
    return


def test_tf_init_with_no_opts(mock_config):
    bucket = '-backend-config=bucket=123456789012-myproj-tfstate'
    key = '-backend-config=key=myenvname-a.tfstate'
    expected = ['terraform',
                'init',
                '-backend=true',
                bucket,
                key,
                '-backend-config=region=us-east-1',
                '-backend-config=profile=my_aws_profile']
    result = tf.Command('init', mock_config, None)
    assert expected == result
    return


def test_tf_init_with_1_opt(mock_config):
    bucket = '-backend-config=bucket=123456789012-myproj-tfstate'
    key = '-backend-config=key=myenvname-a.tfstate'
    tf_opts = [ '-reconfigure' ]
    expected = ['terraform',
                'init',
                '-backend=true',
                bucket,
                key,
                '-backend-config=region=us-east-1',
                '-backend-config=profile=my_aws_profile'] + tf_opts
    result = tf.Command('init', mock_config, tf_opts)
    assert expected == result
    return


def test_tf_push():
    tf_opts = ['push', '.terraform/terraform.tfstate']
    expected = ['terraform', 'state'] + tf_opts
    result = tf.Command('state', None, tf_opts)
    assert expected == result
    return


def test_tf_pull():
    tf_opts = ['pull']
    expected = ['terraform', 'state'] + tf_opts
    result = tf.Command('state', None, tf_opts)
    assert expected == result
    return


def test_tf_validate(mock_config):
    expected = ['terraform',
                'validate']
    result = tf.Command('validate')
    assert expected == result
    return


def test_tf_plan_with_no_opts(mock_config):
    expected = ['terraform', 'plan',
                "-var=aws_region=us-east-1",
                "-var-file=/tmp/test_tmp_dir/vars.tf"]
    result = tf.Command('plan', mock_config)
    assert result == expected
    return


def test_tf_plan_with_destroy(mock_config):
    expected = ['terraform',
                'plan',
                "-var=aws_region=us-east-1",
                '-var-file=/tmp/test_tmp_dir/vars.tf',
                '-destroy',
                ]
    result = tf.Command('plan', mock_config, ['-destroy'] )
    assert result == expected
    return


def test_tf_apply_with_std_opts(mock_config):
    expected = ['terraform',
                'apply',
                '-auto-approve',
                "-var=aws_region=us-east-1",
                '-var-file=/tmp/test_tmp_dir/vars.tf',
                ]
    result = tf.Command('apply',mock_config)
    assert result == expected
    return


def test_tf_apply_with_more_opts(mock_config):
    tf_opts = ['-compact-warnings', '-backup=/foo/bar']
    expected = ['terraform',
                'apply',
                '-auto-approve',
                "-var=aws_region=us-east-1",
                '-var-file=/tmp/test_tmp_dir/vars.tf',
                ] + tf_opts
    result = tf.Command('apply', mock_config, tf_opts )
    assert result == expected
    return


def test_tf_destroy_with_std_opts(mock_config):
    expected = ['terraform',
                'destroy',
                '-auto-approve',
                "-var=aws_region=us-east-1",
                '-var-file=/tmp/test_tmp_dir/vars.tf',
                ]
    result = tf.Command('destroy', mock_config)
    assert result == expected
    return


def test_tf_destroy_with_more_opts(mock_config):
    tf_opts = ['-compact-warnings', '-backup=/foo/bar']
    expected = ['terraform',
                'destroy',
                '-auto-approve',
                "-var=aws_region=us-east-1",
                '-var-file=/tmp/test_tmp_dir/vars.tf',
                ] + tf_opts
    result = tf.Command('destroy', mock_config, tf_opts )
    assert result == expected
    return


def test_tf_output():
    tf_opts = [ 'foo' ]
    expected = ['terraform',
                'output',
                'foo',
                ]
    result = tf.Command('output', None, tf_opts)
    assert result == expected
    return


def test_tf_refresh_1(mock_config):

    result = tf.Command('refresh', mock_config, None)
    expected = [ 'terraform',
                 'refresh',
                 '-var=aws_region=us-east-1',
                 '-var-file=/tmp/test_tmp_dir/vars.tf' ]
    assert result == expected


def test_tf_refresh_2(mock_config):
    tf_opts = [ '-target=foo' ]
    result = tf.Command('refresh', mock_config, tf_opts)

    expected = [ 'terraform',
                 'refresh',
                 '-var=aws_region=us-east-1',
                 '-var-file=/tmp/test_tmp_dir/vars.tf' ] + tf_opts

    assert result == expected


def test_tf_refresh_3(mock_config):
    tf_opts = [ '-target=foo', '-target=bar']
    result = tf.Command('refresh', mock_config, tf_opts)
    expected = [ 'terraform',
                 'refresh',
                 '-var=aws_region=us-east-1',
                 '-var-file=/tmp/test_tmp_dir/vars.tf' ] + tf_opts
    assert result == expected


def test_state_import_std_opts(mock_config):
    objaddr = 'load_balancers.aws_lb_target_group.tg'
    aws_id = "arn:aws:elasticloadbalancing:us-east-1:123456789012:targetgroup/mytarget-group/b27fdd067a3ce1d4"
    expected = [ 'terraform',
                 'import',
                 '-var=aws_region=us-east-1',
                 '-var-file=/tmp/test_tmp_dir/vars.tf',
                 objaddr,
                 aws_id]

    result = tf.Command('import', mock_config, [ objaddr, aws_id ])
    assert result == expected


def test_state_list_1(mock_config):
    expected = [ 'terraform',
                 'state',
                 'list']

    result = tf.Command('state', mock_config, [ 'list' ] )
    assert result == expected


def test_state_list_2(mock_config):
    tf_opts = [ 'list', 'load_balancers.aws_lb_target_group.tg' ]
    expected = [ 'terraform',
                 'state'
                 ] + tf_opts

    result = tf.Command('state', mock_config, tf_opts)
    assert result == expected


def test_state_rm_1(mock_config):
    tf_opts = [ 'rm', 'load_balancers.aws_lb_target_group.tg' ]
    result = tf.Command('state', mock_config, tf_opts)
    expected = [ 'terraform',
                 'state'] + tf_opts
    assert result == expected


def test_state_rm_2(mock_config):
    tf_opts = ['rm',
               'load_balancers.aws_lb_target_group.tg',
               'load_balancers.aws_lb_target_group.tg2']

    result = tf.Command('state', mock_config, tf_opts)
    expected = [ 'terraform',
                 'state' ] + tf_opts
    assert result == expected

