'''
Unit tests for the environments.py module.
'''
import boto3
import json
import pytest
import logging
from   mock import patch
from   moto import ( mock_ec2,
                     mock_s3 )
import os

from   deployer.exceptions    import EnvironmentExistsException

import tests.MyBoto3 as MyBoto3
from deployer.environments import Environment

logger = logging.getLogger(os.path.basename('deployer'))
fake_boto3 = MyBoto3.MyBoto3()


def mock_run_cmd(args, cwd=None):
    print("CWD: {}, Running command: {}".format(cwd, " ".join(args)))
    return 0


def mock_inst_is_running(instance_id):
    return True


@pytest.fixture
def mock_creds(scope="function"):
    os.environ['AWS_ACCESS_KEY_ID']     = 'testing'
    os.environ['AWS_SECRET_ACCESS_KEY'] = 'testing'
    os.environ['AWS_SECURITY_TOKEN']    = 'testing'
    os.environ['AWS_SESSION_TOKEN']     = 'testing'

    return os.environ


@pytest.fixture
def mock_config(scope="function"):
    return {
        "terraform": "git@gitlab.org:group/project.git?branch=made_up_branch",
        "aws_profile": "tests-random",
        "aws_region": "us-east-1",
        "availability_zones": [
            'us-east-1b',
            'us-east-1c',
            'us-east-1d',
            'us-east-1e'
        ],
        "account_id": "123456789012",
        "environment": {
            "name": "myenvname",
            "version": "a",
        },
        'tags': {
            'system_type' : 'mock_product'
        },
        "env_name": "myenvname-a",
        "tf_state": "myenvname-a.tfstate",
        "tf_state_bucket": "123456789012-myproj-tfstate",
        "project_config": "123456789012-myproj-data",
        "project": 'myproj',
        "tfvars" : '/tmp/test_tmp_dir/vars.tf',
        "tf_root": '/tmp/test_tmp_dir/terraform',
        "tmpdir" : '/tmp/test_tmp_dir'
    }


@mock_ec2
def mock_vpcs(scope="function"):
    ec2c = boto3.client('ec2',
                        region_name='us-east-1',
                        aws_access_key_id='',
                        aws_secret_access_key='',
                        aws_session_token='')
    vpc1 = ec2c.create_vpc(CidrBlock='10.1.0.0/16').get('Vpc').get('VpcId')
    vpc2 = ec2c.create_vpc(CidrBlock='10.2.0.0/16').get('Vpc').get('VpcId')
    vpc3 = ec2c.create_vpc(CidrBlock='10.3.0.0/16').get('Vpc').get('VpcId')
    ec2c.create_tags(Resources = [ vpc1 ],
                     Tags=[ {'Key':'Name',
                             'Value' : 'myproj-myenvname-a'},
                            {'Key':'env',
                             'Value' : 'myenvname-a'} ])
    ec2c.create_tags(Resources = [ vpc2 ],
                     Tags=[ {'Key':'Name',
                             'Value' : 'myproj-myenvname-b'},
                            {'Key':'env',
                             'Value' : 'myenvname-b'} ])
    ec2c.create_tags(Resources = [ vpc3 ],
                     Tags=[ {'Key':'Name',
                             'Value' : 'myproj-myenvname-c'},
                            {'Key':'env',
                             'Value' : 'myenvname-c'} ])
    return ec2c


@mock_s3
@mock_ec2
def test_create_env_exists_1(mock_config, mock_creds):
    expected_arn = [ "arn:aws:ec2:us-east-1:419934374614:instance/i-c3bef428" ]
    expected_msg = "\n\nAn environment with the name {} already exists."
    expected_msg += "\nPlease tear it down before trying to rebuild."
    expected_msg += "\n\n{}".format(json.dumps(expected_arn, indent=4))

    env_name = "-".join([ mock_config['environment']['name'],
                          mock_config['environment']['version'] ])

    if 'tags' in mock_config and 'system_type' in mock_config['tags']:
        env_name = "-".join([mock_config['tags']['system_type'], env_name ])

    s3client = boto3.client('s3')

    s3client.create_bucket(Bucket="123456789012-myproj-tfstate")

    ec2c = boto3.client('ec2')
    vpc1 = ec2c.create_vpc(CidrBlock='10.1.0.0/16').get('Vpc').get('VpcId')
    ec2c.create_tags(Resources = [ vpc1 ],
                     Tags=[ {'Key':'Name',
                             'Value' : 'myproj-myenvname-a'},
                            {'Key':'env',
                             'Value' : 'myenvname-a'},
                            {'Key' : 'system_type',
                             'Value' : 'mock_product'}] )

    with pytest.raises(EnvironmentExistsException) as e:
        with patch('deployer.aws.instance_is_running', mock_inst_is_running):
            with patch('deployer.utils.run_command', mock_run_cmd):
                with patch('deployer.aws.boto3', fake_boto3):
                    env = Environment(mock_config)
                    #pytest.set_trace()
                    env.create()

    from termcolor import colored
    assert (e.value.args[0] == colored(expected_msg.format(env_name), 'red'))

    return


@mock_s3
@mock_ec2
def test_create_env_does_not_exist(mock_config):
    mock_config['environment']['name'] = 'myotherenvname'
    mock_config['environment']['version'] = 'z'
    s3client = boto3.client('s3')
    s3client.create_bucket(Bucket="123456789012-myproj-tfstate")
    with patch('deployer.utils.run_command', mock_run_cmd):
        with patch('deployer.aws.boto3', fake_boto3):
            env = Environment(mock_config)
            assert env.create()

    return


@mock_s3
@mock_ec2
def test_create_env_exists_force(caplog, mock_config, mock_creds):

    s3client = boto3.client('s3')
    s3client.create_bucket(Bucket="123456789012-myproj-tfstate")

    ec2c = boto3.client('ec2')
    vpc1 = ec2c.create_vpc(CidrBlock='10.1.0.0/16').get('Vpc').get('VpcId')
    ec2c.create_tags(Resources = [ vpc1 ],
                     Tags=[ {'Key':'Name',
                             'Value' : 'myproj-myenvname-a'},
                            {'Key':'env',
                             'Value' : 'myenvname-a'},
                            {'Key' : 'system_type',
                             'Value' : 'mock_product'}] )
    logging.captureWarnings(True)

    with patch('deployer.aws.instance_is_running', mock_inst_is_running):
        with patch('deployer.utils.run_command', mock_run_cmd):
            with patch('deployer.aws.boto3', fake_boto3):
                with caplog.at_level(logging.WARNING,
                                     logger="root.deployer"):
                    env = Environment(mock_config)
                    env.create(force = True)

    expected_arn = "arn:aws:ec2:us-east-1:419934374614:instance/i-c3bef428"
    env_name = mock_config['env_name']
    if 'tags' in mock_config and 'system_type' in mock_config['tags']:
        env_name = "-".join([mock_config['tags']['system_type'], env_name ])

    assert ("WARNING" in caplog.text)
    assert ("An environment with the name {}".format(env_name) in caplog.text)
    assert ("already exists." in caplog.text)
    assert (expected_arn in caplog.text)
    creating = "Creating environment with the name {} anyway.".format(env_name)
    assert ( creating in caplog.text)
    dupes = "You may have resources with duplicate names."
    assert ( dupes in caplog.text)
    assert ("--force detected..." in caplog.text)

    return


@mock_s3
@mock_ec2
def test_create_env_does_not_exists_force(mock_config):
    mock_config['environment']['name'] = 'myotherenvname'
    mock_config['environment']['version'] = 'z'
    s3client = boto3.client('s3')
    s3client.create_bucket(Bucket="123456789012-myproj-tfstate")
    with patch('deployer.utils.run_command', mock_run_cmd):
        with patch('deployer.aws.boto3', fake_boto3):
            env = Environment(mock_config)
            assert env.create(force = True)

    return


@mock_ec2
def test_list_deployed_environment_versions(mock_config):
    mock_vpcs()
    #env_name = mock_config['environment']['name']
    with patch('deployer.utils.run_command', mock_run_cmd):
        with patch('deployer.aws.boto3', fake_boto3):
            env = Environment(mock_config)
            existing_env_versions = env.list_deployed_environment_versions()

    assert existing_env_versions == [ 'a', 'b', 'c' ]
    return


@mock_ec2
def test_get_next_env_version_no_ephemeral(mock_config):
    mock_vpcs()
    expected = 'd'
    with patch('deployer.utils.run_command', mock_run_cmd):
        with patch('deployer.aws.boto3', fake_boto3):
            with patch('deployer.aws.instance_is_running',
                       mock_inst_is_running):
                env = Environment(mock_config)
                next_version = env.get_next_version()

    assert expected == next_version


@mock_ec2
def test_get_next_env_version_with_ephemeral(mock_config):
    mock_vpcs()
    expected = 'd'
    with patch('deployer.utils.run_command', mock_run_cmd):
        with patch('deployer.aws.boto3', fake_boto3):
            with patch('deployer.aws.instance_is_running',
                       mock_inst_is_running):
                env = Environment(mock_config)
                next_version = env.get_next_version(ephemeral_env='mock_product')
    assert expected == next_version


@mock_ec2
def test_get_next_env_version_with_no_config(mock_config):
    mock_vpcs()
    expected = 'd'
    with patch('deployer.utils.run_command', mock_run_cmd):
        with patch('deployer.aws.boto3', fake_boto3):
            with patch('deployer.aws.instance_is_running',
                       mock_inst_is_running):

                env = Environment()
                next_version = env.get_next_version(env_name="myenvname",
                                                    ephemeral_env='mock_product')
    assert expected == next_version


@mock_ec2
def test_get_next_env_version_with_no_config_env_name_fail(caplog, mock_config):
    mock_vpcs()
    expected = 'd'

    logging.captureWarnings(True)
    with patch('deployer.utils.run_command', mock_run_cmd):
        with patch('deployer.aws.boto3', fake_boto3):
            with patch('deployer.aws.instance_is_running',
                       mock_inst_is_running):
                with caplog.at_level(logging.CRITICAL,
                                     logger="root.deployer"):
                    with pytest.raises(SystemExit) as pytest_wrapped_e:
                        env = Environment()
                        next_version = env.get_next_version()

    assert ("CRITICAL" in caplog.text)
    assert ("env_name not set!" in caplog.text)
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 1

