import boto3
import json
import os
import pytest
import deep_merge
from mock import patch
from moto import mock_s3
from moto import mock_ec2


import deployer.aws          as aws
import deployer.utils        as utils
from   deployer.preflight    import write_vars
#import deployer.environments as env
from deployer.environments import Environment

import tests.MyBoto3 as MyBoto3
fake_boto3 = MyBoto3.MyBoto3()

top_dir = os.path.join(os.path.dirname(__file__),
                       os.pardir,
                       os.pardir)
deployer_dir  = os.path.join(top_dir, 'deployer')
conf_dir      = os.path.join(deployer_dir, 'conf')
test_dir      = os.path.join(top_dir, 'tests')
test_data_dir = os.path.join(test_dir, 'test_data')


class mock_opts(object):

    def __init__(self):
        return

    config = [ os.path.join(test_data_dir, "app_vars_data.json"),
               os.path.join(test_data_dir, "common_vars_data.json") ]

    outfile = 'config.json'

    override = {'tmpdir': '/tmp/foo', 'tags': {'foo': 'bar'}}

    schema = os.path.join(conf_dir, "default_schema.json")


@pytest.fixture
def opts(scope="function"):
    return mock_opts()


def mock_environment_exists(env, next_env, ephemeral_env):
    return False


@mock_s3
@mock_ec2
def test_gen_config_main(opts):

    boto3.setup_default_session(profile_name="tests-random",
                                region_name='us-east-1',
                                aws_access_key_id='',
                                aws_secret_access_key='')

    # Load the varfile and validate that it meets the required schema
    config = utils.merge_vars(opts.config)

    # Validate initial config
    utils.validate_schema(config, opts.schema)

    newConfig = config
    if opts.override:
        newConfig = deep_merge.merge(config, opts.override)

    with patch('deployer.aws.environment_exists', mock_environment_exists):
        if ( 'version' in newConfig['environment'].keys() and
             newConfig['environment']['version'] == 'GET_NEXT'):
            env_name = newConfig['environment']['name']
            prod = newConfig['tags'].get('product', None)

            env = Environment()
            next_version = env.get_next_version(env_name, prod)
            newConfig['environment']['version'] = next_version

    # fake_boto3 still required here because mock_iam has not yet
    # implemented the list_account_aliases() method yet, which is used
    # in aws.configure().
    os.environ['AWS_ACCESS_KEY_ID'] = 'foo'
    os.environ['AWS_SECRET_ACCESS_KEY'] = 'bar'
    mock = mock_s3()
    mock.start()
    s3client = boto3.client('s3')

    s3client.create_bucket(Bucket="123456789012-myproj-data")
    s3client.create_bucket(Bucket="123456789012-myproj-tfstate")
    with patch('deployer.aws.boto3', fake_boto3):
        # We need to create the bucket since this is all in Moto's 'virtual'
        # AWS account
        newConfig = aws.configure(newConfig)

    newConfig = utils.eval_config(newConfig)

    # Validate final config passed to terraform before we write it out
    utils.validate_schema(newConfig, opts.schema)
    write_vars(newConfig, varfile=opts.outfile)

    with open(opts.outfile,"r") as gen_config_file:
        gen_config = json.load(gen_config_file)

    expected_config_path = os.path.join(test_data_dir,
                                        "gen_config_expected_outfile.json")
    
    with open(expected_config_path, "r") as expected_config_file:
        expected_config = json.load(expected_config_file)

    (added, removed, modified, same) = dict_compare(expected_config,
                                                    gen_config)

    # There should be no "added" keys
    assert len(added) == 0

    # Or "removed" keys
    assert len(removed) == 0

    # Or "modified" keys.
    assert len(modified.keys()) == 0

    # But all keys should be the "same"
    assert len(same) == len(expected_config.keys()) == len(gen_config.keys())

    for item in same:
        assert expected_config[item] == gen_config[item]

    return


def dict_compare(d1, d2):
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    shared_keys = d1_keys.intersection(d2_keys)
    added = d1_keys - d2_keys
    removed = d2_keys - d1_keys
    modified = {o : (d1[o], d2[o]) for o in shared_keys if d1[o] != d2[o]}
    same = set(o for o in shared_keys if d1[o] == d2[o])
    return added, removed, modified, same
